package fibonacciTest;

import fibonacci.FibonacciSet;
import number.NumMagic;
import out.ConsoleOut;

import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        int intervalA=0;
        int intervalB=0;
        int fibonacciN=0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter the interval A & B: ");
        NumMagic.setIntervalAndProcess(intervalA=sc.nextInt(),intervalB=sc.nextInt());
        System.out.println("Odd numbers in interval "+intervalA+"-"+intervalB);
        ConsoleOut.PrintList(NumMagic.getOddList());
        System.out.println("Even numbers in interval "+intervalB+"-"+intervalA);
        ConsoleOut.PrintList(NumMagic.getEvenList());
        System.out.println("Sum of odd numbers: "+NumMagic.getOddSum());
        System.out.println("Sum of even numbers: "+NumMagic.getEvenSum());
        FibonacciSet fibSet = new FibonacciSet(NumMagic.getOddMax(),NumMagic.getEvenMax());
        System.out.println("Enter the N. N - size of fibonacci set");
        ConsoleOut.PrintList(fibSet.getSet(fibonacciN=sc.nextInt()));
        System.out.println(fibSet.getNumOfOdd());
        System.out.println("Percent of Odd number: "+100*(double)fibSet.getNumOfOdd()/(double)fibonacciN);
        System.out.println("Percent of Even number: "+100*((double)fibonacciN-fibSet.getNumOfOdd())/(double)fibonacciN);
    }
}
