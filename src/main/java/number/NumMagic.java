package number;

import java.util.ArrayList;

/**
 * Class NumMagic with fields <b>intervalA</b> <b>intervalB</b> <b>oddList</b> <b>evenList</b> <b>odd + even Sum</b>
 * @author vikto
 * @version 1.0
 */
public class NumMagic {
    /** Field intervalA */
    private static int intervalA;
    /** Field intervalB */
    private static int intervalB;
    /** Field oddList */
    private static ArrayList<Integer> oddList;
    /** Field evenList */
    private static ArrayList<Integer> evenList;
    /** Field oddSum */
    private static int oddSum;
    /** Field evenSum */
    private static int evenSum;

    /**
     * Initialize method
     * @param A - start of interval
     * @param B - end of interval
     */
    public static void setIntervalAndProcess(int A,int B){
        intervalA = A;
        intervalB = B;
        setOddList();
        setEvenList();
    }
    /**
     * Method fills in an array of odd numbers
     */
    private static void setOddList(){
        oddList = new ArrayList<Integer>();
        oddSum =0;
        for(int ptr=intervalA; ptr < intervalB; ptr++){
            if((ptr&1)!=0){
                oddList.add(ptr);
                oddSum +=ptr;
            }
        }
    }
    /**
     * Method fills in an array of even numbers
     */
    private static void setEvenList(){
        evenList = new ArrayList<Integer>();
        evenSum=0;
        for(int ptr=intervalB; ptr > intervalA; ptr--){
            if((ptr&1)==0){
                evenList.add(ptr);
                evenSum+=ptr;
            }
        }
    }
    /**
     * Getter method for oddList
     * @return list with odd numbers
     */
    public static ArrayList<Integer> getOddList(){
        return oddList;
    }
    /**
     * Getter method for evenList
     * @return list with even numbers
     */
    public static ArrayList<Integer> getEvenList(){
        return evenList;
    }
    /**
     * Getter method for oddList sum
     * @return sum of all odd numbers
     */
    public static int getOddSum(){
        return oddSum;
    }
    /**
     * Getter method for evenList sum
     * @return sum of all even numbers
     */
    public static int getEvenSum(){
        return evenSum;
    }
    /**
     * Getter method for Odd maximum
     * @return max number of Odd list
     */
    public static int getOddMax(){
        return oddList.get(evenList.size()-1);
    }
    /**
     * Getter method for Even maximum
     * @return max number of Even list
     */
    public static int getEvenMax(){
        return evenList.get(0);
    }
}
