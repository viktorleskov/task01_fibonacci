package out;

import java.util.List;

/**
 * Custom output for List
 * @author vikto
 * @version 1.0
 */
public class ConsoleOut {
    /**
     * Method lists all items in the list
     * @param ls - list of numbers
     */
    public static void PrintList(List ls){
        for(int ptr=0;ptr < ls.size(); ptr++){
            System.out.print(ls.get(ptr)+"\t");
        }
        System.out.println(" ");
    }

}
