package fibonacci;

import java.util.ArrayList;

/**
 * Class FibonacciSet with fields <b>f1</b>,<b>f2</b>,<b>fiboSet</b>
 * @author vikto
 * @version 1.0
 */
public class FibonacciSet {
    /** Field f1 - first element of fiboSet */
    private final Integer f1;
    /** Field f2 - second element of fiboSet */
    private final Integer f2;
    /** Field fiboSet - list of fibonacci numbers */
    private ArrayList<Integer> fiboSet;

    /**
     * Constructor - create new object with special values
     * @param startInt - first element of fibonacci set
     * @param endInt - second element of fibonacci set
     */
    public FibonacciSet(int startInt, int endInt){
        f1=startInt;
        f2=endInt;
    }
    /**
     * Method for filling fibonacci set
     * @param sizeSet - size of array
     * @return filled arrayList with elements
     */
    private ArrayList<Integer> makeSet(int sizeSet){
        fiboSet=new ArrayList<Integer>();
        fiboSet.add(f1);
        fiboSet.add(f2);
        for(int iter=fiboSet.size(); iter<sizeSet; iter++){
            fiboSet.add(fiboSet.get(iter-1)+fiboSet.get(iter-2));
        }
        return fiboSet;
    }
    /**
     * Method for counting odd numbers in fibonacci List
     * @return the number of odd numbers in the list
     */
    public int getNumOfOdd(){
        int numOfOdd=0;
        for(int ptr=0; ptr < fiboSet.size(); ptr++){
            if((fiboSet.get(ptr)&1)!=0){
                numOfOdd++;
            }
        }
        return numOfOdd;
    }
    /**
     * Getter method for fibonacci List
     * @param sizeSet - size of new fibonacci List
     * @return new fibonacci list
     */
    public ArrayList<Integer> getSet(int sizeSet){
        return makeSet(sizeSet);
    }
    /**
     * Getter method for fibonacci List
     * @return fibonacci list
     */
    public ArrayList<Integer> getSet(){
        return fiboSet;
    }
}
